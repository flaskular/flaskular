from setuptools import setup

from flaskular_manage import (FlaskularInstall, FlaskularDevelop,
                              FlaskularPyTest, FlaskularPyDoc)


class Install(FlaskularInstall):
    pass


class Develop(FlaskularDevelop):
    pass


class PyTest(FlaskularPyTest):
    pass


class PyDoc(FlaskularPyDoc):
    pass


setup(name='flaskular',
      version='0.1.0',
      packages=['flaskular', 'flaskular_manage'],
      include_package_data=True,
      install_requires=[
          'sqlalchemy==1.1.9',
          'flask==0.12.1',
          'flask-security==1.7.5',
          'flask-sqlalchemy==2.2',
          'flask-testing',
          'psycopg2==2.7.1',
          'alembic==0.9.1',
          'Flask-Migrate',
          'bcrypt==3.1.3',
          'nltk==3.2.2',
          'requests==2.14.2',
          'uwsgi',
          'colored==1.3.5'
      ],
      tests_require=[
          'pytest',
          'pytest-cov',
      ],
      cmdclass={
          'install': Install,
          'develop': Develop,
          'test': PyTest,
          'document': PyDoc
      })
