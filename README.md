A framework running Flask as a backend and AngularJS as a front-end.

See [the flaskular-bootstrap project](https://gitlab.com/flaskular/flaskular-bootstrap)
for installation, getting started, and usage instructions.

# Developing with Flaskular #

## Existing Backend Modules ##

1. angular

    * Models:

        * TODO (coming soon)

## Extending Flaskular Backend Models ##

## Overwriting Flaskular Routes ##

Only routes (either in Flaskular or in the extending app) that have been
flagged as `overwritable` can be overwritten. Since all routes in Flaskular
have been flagged as `overwritable`, they can all be overwritten.

The easiest way to overwrite a flaskular route is to import the Flaskular
module is to import the Blueprint of the flaskular module into the app, define
the overwriting route on that blueprint, using the same url and the same
function name, and then ensuring that the Blueprint is re-registered in the
app's factory.

As one example, the index route defined in the flaskular module `angular` is
overwritten in `flaskular-bootstrap` (and should be in any other app) in order
to make flaskular aware of the flaskular-bootstrap css and js scripts.

Example:

        # --- flaskular/somemodule/controllers.py ---
        #   (an existing flaskular module, you don't add/change anything here)
        from flask import Blueprint

        somemodule = Blueprint('somemodule', __name__)

        @somemodule.route('/my-route', overwritable=True)
        def my_route():
            # Flaskular code here
            # ...


        # --- flaskular/factory.py ---
        #   (existing, do not add/change anything here)
        from flaskular.somemodule.controllers import somemodule

        def register_blueprints(app):
            app.register_blueprint(somemodule)


        # --- app/somemodule/controllers.py ---
        #   (this is code you write)
        from flaskular.somemodule.controllers import somemodule

        @somemodule.route('/my-route')
        def my_route():
            """Overwrites Flaskulars /my-route route.

            Note, this route cannot be overwritten again unless you pass
            `overwritable=True` as a parameter into `somemodule.route()`
            here too.
            """
            # Your custom code here
            # ...


        # --- app/facctory.py ---
        #   Don't forget to re-register the blueprint, imported this time from
        #   app. Otherwise it will use the flaskular route and not the custom
        #   route.
        from app.somemodule.controllers import somemodule

        def register_blueprints(app):
            app.register_blueprint(somemodule)


If you either cannot or do not wish to use the same blueprint and/or function
name in the overwriting route, then you will need to pass the endpoint name
of the flaskular route that is to be overwritten as a parameter into `route()`
of the new route. This endpoint will be as string named
`[module name].[function name]`. It is up to you to also ensure that the
urls are the same (taking into account the optional url previxes available
to Blueprints).

This strategy is used to test the overwriting capability in
`flaskular.core.test_flaskular.py`. It is unlikely that it will be needed
outside a testing environment.

Example (using the same flaskular route as above):

        # --- app/othermodule/controllers.py ---
        #   (this is code you write)
        from flask import Blueprint

        othermodule = Blueprint('othermodule', __name__)

        @othermodule.route('/my-route', endpoint='somemodule.my_route')
        def other_route():
            # Your custom code here
            # ...
