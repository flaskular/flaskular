import os

from flask_security import SQLAlchemyUserDatastore

from flaskular import Flaskular
from flaskular.extensions import db, security, print_warning
from flaskular.extensions import RegistrationManager as rm

from flaskular.angular.controllers import angular
from flaskular.auth.controllers import auth


def register_blueprints(app):
    """Utility to register all wired-in blueprints for any app.

    Parameters
    ----------
    app : Flask
        The flask app with which to register blueprints.

    Returns
    -------
    app : Flask
        The flask app in which the blueprints have been registered.
    """
    # Register module blueprints
    app.register_blueprint(auth)
    app.register_blueprint(angular)

    return app


def configure_app(User, Role, app=None, testing=False):
    """Factory to configure the flaskular app.

    If the app is not given, it is created internally (usually only for testing
    purposes).

    Note: To reference the current app from within a module:

            from flask import current_app

    Parameters
    ----------
    User : class
        The user model class. User.initialize() will be called on app creation
        in order to create default users, etc. Passed as a parameter so that
        the app extended User is used, not the flaskular User.
    Role : class
        The role model class. Role.initialize() will be called on app creation
        in order to create default roles, etc. Passed as a parameter for the
        same reason as User.
    app : Flask or None
        If not none, a handle to a previously created app. If none, it is
        created internally. Should be given if `create_app` is handled by
        an app using `flaskular`. Should not be given for internal `flaskular`
        testing.
    testing : bool, default=False
        True if the testing configuration and settings should be used.

    Returns
    -------
    app : Flask
        The flask app, configured.
    """
    # Setup Application
    if app is None:
        app = Flaskular(__name__)

    settings = os.environ['APP_SETTINGS']
    if testing:
        settings += 'Testing'

    app.config.from_object(settings)

    default_key = 'changeme'
    if app.config['SECRET_KEY'] == default_key:
        print_warning(('SECRET_KEY is still using default "{}". For security '
                       'purposes, please change this in config.py'
                       ).format(default_key))
    if app.config['CSRF_SESSION_KEY'] == default_key:
        print_warning(('CSRF_SESSION_KEY is still using default "{}". For '
                       'security purposes, please change this in config.py'
                       ).format(default_key))

    # Setup Database
    db.init_app(app)

    # Setup Security
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, user_datastore)

    # Register blueprint(s)
    app = register_blueprints(app)

    # Register User and Role
    rm.User = User
    rm.Role = Role

    # Create test user if no user exists
    with app.app_context():
        db.create_all()

        # Create roles if they do not exist
        Role.initialize()

        # Create admin user if it does not exist
        User.initialize()

    return app
