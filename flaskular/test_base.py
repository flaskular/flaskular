import json
from functools import partial

from flask.ext.testing import TestCase

from flaskular.auth import FlaskularUser, FlaskularRole
from flaskular.factory import configure_app, db

default_create = partial(configure_app, User=FlaskularUser, Role=FlaskularRole)


class BaseTest(TestCase):
    """Parent of TestModel and TestController.

    Attributes
    ----------
    _create_app : function
        A handle to the function to create the app. Should only take in 1
        parameter (testing). If more is needed, build a partial filling in
        the other parameters and pass that partial.
    """

    _create_app = default_create

    def create_app(self):
        return self.__class__._create_app(testing=True)

    def setUp(self):
        db.create_all()

    def tearDown(self):
        db.session.remove()
        db.drop_all()


class TestModel(BaseTest):
    """Base class for tests on flask models.

    Sets up the app and db before each test is run and tears them back down at
    the end (clearing the test db completely).
    """

    pass  # Right now, doesn't add anything beyond BaseTest. May in the future.


class TestController(BaseTest):
    """Base class for tests on flask controllers.

    Sets up the app and db before each test is run and tears them back down at
    the end (clearing the test db completely).

    Also defines helper functions that make testing json posts and responses
    simpler.
    """

    def setup(self):
        BaseTest.setup(self)
        self.client = self.app.test_client()

    def post(self, url, follow_redirects=True, **kwargs):
        """
        Simulates a json post to and json response from the server (since this
        is an angular app, we can expect all post requests to be like this).

        Parameters
        ----------
        url : str
            The post url, relative to the host:port.
        follow_redirects : bool
            Sets the test_client.post parameter of the same name.
        kwargs
            Pass in the key/value pairs that will be sent as data in the json
            request.

        Returns
        -------
        data : dict
            The python dictionary of the json data sent by the response.
        status_code : int
            The status code of the response
        """
        response = self.client.post(
            url,
            data=json.dumps(kwargs),
            follow_redirects=follow_redirects,
            content_type='application/json;charset=UTF-8'
        )
        status_code = response.status_code
        data = json.loads(response.data.decode('utf-8'))

        return data, status_code

    def get(self, url, follow_redirects=True):
        """
        Simulates a get with a json response from the server (since this is
        an angular app, we can expect all get requests to be like this).

        Parameters
        ----------
        url : str
            The get url, relative to the host:port.
        follow_redirects : bool
            Sets the test_client.get parameter of the same name.

        Returns
        -------
        data : dict
            The python dictionary of the json data sent by the response.
        status_code : int
            The status code of the response.
        """
        response = self.client.get(
            url,
            follow_redirects=follow_redirects
        )
        status_code = response.status_code
        data = json.loads(response.data.decode('utf-8'))

        return data, status_code
