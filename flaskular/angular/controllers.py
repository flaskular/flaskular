from flask import Blueprint, render_template, send_from_directory

from .models import (list_all_static_js, list_all_static_css,
                     get_abs_static_dir)

###############################################################################
#   Configuration
###############################################################################

angular = Blueprint('angular', __name__)


###############################################################################
#   Routes
###############################################################################

@angular.route('/', overwritable=True)
def index():
    """The index route that renders the core angular page.

    Angular takes over from here, making calls back to flask as necessary.
    """
    return render_template('angular/index.html',
                           static_js=list_all_static_js(),
                           static_css=list_all_static_css())


@angular.route('/core/<path:filename>')
def core_static(filename):
    static_dir = get_abs_static_dir('static', __file__)
    return send_from_directory(static_dir, filename)


@angular.route('/corelib/<path:filename>')
def core_lib(filename):
    lib_dir = get_abs_static_dir('jslib', __file__)
    return send_from_directory(lib_dir, filename)
