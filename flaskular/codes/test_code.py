"""
Unit tests for the codes module.
"""
import pytest

from . import Code, MissingArgumentError


class TestCode:
    """
    Tests `app.codes.codemanager.Code`.
    """

    def test_no_argument(self):
        code = Code('test_no_argument', 'This is a test.')

        assert code.name == 'test_no_argument'
        assert code.msg == 'This is a test.'

    def test_one_argument(self):
        code = Code('test_one_argument', 'One argument: {arg}',
                    arg='Something')

        assert code.name == 'test_one_argument'
        assert code.msg == 'One argument: Something'

    def test_two_arguments(self):
        code = Code('test_two_arguments', '({lat}, {lon})', lat=40, lon=-90)

        assert code.name == 'test_two_arguments'
        assert code.msg == '(40, -90)'

    def test_missing_argument(self):

        with pytest.raises(MissingArgumentError) as excinfo:
            Code('test_missing_argument', '({lat}, {lon})', lat=40)

        assert excinfo.value.code_name == 'test_missing_argument'
        assert excinfo.value.missing_argument == '\'lon\''

    def test_equality_no_args(self):
        c1 = Code('test_1', 'A message')
        c2 = Code('test_1', 'A message')

        assert c1 == c2

    def test_inequality_no_args(self):
        c1 = Code('test_1', 'A message')
        c2 = Code('test_2', 'Another message')

        assert c1 != c2

    def test_equality_args(self):
        c1 = Code('test_1', 'A message {a} {b}', a=1, b=2)
        c2 = Code('test_1', 'A message {a} {b}', a=3, b=4)

        assert c1 == c2

    def test_inequality_args(self):
        c1 = Code('test_1', 'A message {a} {b}', a=1, b=2)
        c2 = Code('test_2', 'A message {a} {b}', a=1, b=2)

        assert c1 != c2

    def test_equality_wrong_type(self):
        c = Code('test_code', 'A message.')

        assert not (c == 2)
        assert c != 2

    def test_representation(self):
        c = Code('test_code', 'A message {a}.', a='here')

        assert str(c) == '<Code> test_code: "A message here."'
