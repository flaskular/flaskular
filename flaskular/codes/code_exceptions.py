

class MissingArgumentError(Exception):
    """
    Defines the error if a code is missing an argument required by the specs
    of the code.

    Parameters
    ----------
    code_name : str
        The name of the code invoking this exception.
    missing_argument : str
        The name of the arguemnt that is missing.
    """

    def __init__(self, code_name, missing_argument):
        self.code_name = code_name
        self.missing_argument = missing_argument

    def __str__(self):
        return 'Code "{}" requires the following argument: {}'.format(
            self.code_name, self.missing_argument
        )
