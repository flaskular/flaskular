from . import CodeManager


class TestCodeManager:
    """
    Tests `app.codes.codemanager.CodeManager`.
    """

    def setup(self):
        """
        Caled before each class test cases are executed. Sets up codes with the
        code dictionary given in the CodeManager usage documentation.
        """
        code_dict = {
            'example_success': 'The server succeeded',
            'example_failed': ('We failed. Expected "{expected}", ' +
                               'got "{actual}".')
        }
        self.codes = CodeManager(code_dict)

    def test_example_success(self):
        """
        This is the first example in the CodeManager usage documentation.
        """
        cs = self.codes.example_success()
        assert cs.name == 'example_success'
        assert cs.msg == 'The server succeeded'

    def test_example_failed(self):
        cf = self.codes.example_failed(expected='exp', actual='act')
        assert cf.name == 'example_failed'
        assert cf.msg == 'We failed. Expected "exp", got "act".'
