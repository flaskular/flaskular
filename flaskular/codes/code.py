from .code_exceptions import MissingArgumentError


class Code(object):
    """
    Defines a single code object. One code is equal to another if they share
    the same name, even if the messages have taken different arguments.

    Attributes
    ----------
    name : str
        The name of the code, which is also the name of the function linking
        to the code in CodeManager.
    msg : str
        The (human readable) message to display for the code.

    Paramters
    ---------
    name : str
        Sets the name attribute.
    msg : str
        Sets the message attribute after the wildcards are filled.
    **kwargs
        The values to fill in as the wildcards in the message.
    """

    def __init__(self, name, msg, **kwargs):
        self.name = name
        try:
            self.msg = msg.format(**kwargs)
        except KeyError as ke:
            raise MissingArgumentError(name, str(ke))

    def __repr__(self):
        return '<Code> {}: "{}"'.format(self.name, self.msg)

    def __eq__(self, other):
        """
        Equality is defined by the same name, has nothing to do with msg
        arguments.
        """
        if isinstance(other, Code):
            return self.name == other.name
        return NotImplemented

    def __ne__(self, other):
        result = self.__eq__(other)
        if result is NotImplemented:
            return result
        return not result
