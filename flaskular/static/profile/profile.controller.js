/**
 * The profile controller. Used to change profile info and password.
 *
 * @namespace flaskular.profile
 */
(function() {
    'use strict';

    angular
        .module('flaskular.profile')
        .controller('ProfileController', ProfileController);

    ProfileController.$inject = ['$scope', '$state', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Profile controller definition.
     */
    function ProfileController($scope, $state, authService) {
        var vm = this;

        // Data
        vm.user = {};
        vm.profileLoading = false;
        vm.profileSuccess = false;

        vm.passForm = {};
        vm.passwordLoading = false;
        vm.passwordError = false;
        vm.passwordErrorMessage = '';
        vm.passwordSuccess = false;

        // Methods
        vm.passEqual = passEqual;
        vm.closePasswordError = closePasswordError;
        vm.changePassword = changePassword;
        vm.updateProfile = updateProfile;

        // Initialize
        initialize();

        ////////////////////////////////
        //  Implementations
        ////////////////////////////////

        function initialize() {

            // Handle for success
            function handleSuccess(data) {
                vm.user = data.userDict;
            }

            // Handle for failure
            function handleError(data) {
                $state.go('error', data, {});
            }

            // Fetch the user dictionary
            authService.getUserDict()
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Returns false if password and its repeat are not equal, true
         * otherwise.
         *
         * @return {bool}
         */
        function passEqual() {
            return vm.passForm.password === vm.passForm.repeat;
        }

        /**
         * Closes and clears any password messages/highlights.
         */
        function closePasswordError() {
            vm.passwordError = false;
        }

        /**
         * Attempts to change the password according to the change password
         * form.
         */
        function changePassword() {
            var id = vm.user.id;
            var oldPassword = vm.passForm.oldPassword;
            var newPassword = vm.passForm.password;

            vm.passwordLoading = true;

            // Handle for success
            function handleSuccess(data) {
                vm.passwordLoading = false;
                vm.passwordSuccess = true;

                vm.passForm = {};
            }

            // Handle for error
            function handleError(data) {
                vm.passwordLoading = false;
                vm.passwordErrorMessage = data.msg;
                vm.passwordError = true;
                vm.passForm.oldPassword = '';

                if (data.code !== 'password_change_incorrect_old') {
                    $state.go('error', data, {});
                }
            }

            // Run the attempt to change the password
            authService.changePassword(id, oldPassword, newPassword)
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Attempts to update the user's profile.
         */
        function updateProfile() {
            var id = vm.user.id;
            var firstName = vm.user.firstName;
            var lastName = vm.user.lastName;

            vm.profileLoading = true;

            // Handle for success
            function handleSuccess(data) {
                vm.profileLoading = false;
                vm.profileSuccess = true;
            }

            // Handle for error
            function handleError(data) {
                vm.profileLoading = false;
                $state.go('error', data, {});
            }

            // Run the attempt to change the password
            authService.updateProfile(id, firstName, lastName)
                .then(handleSuccess)
                .catch(handleError);
        }
    }
})();
