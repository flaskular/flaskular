/**
 * Module definition for home.
 *
 * @namespace flaskular.home
 */
(function() {
    'use strict';

    angular
        .module('flaskular.home', [
            'ui.router',
            'flaskular.router',
            'ui.bootstrap'
        ]);
})();
