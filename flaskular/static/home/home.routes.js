/**
 * Routes for the home page.
 */
(function() {
    'use strict';

    angular
        .module('flaskular.home')
        .run(runBlock);

    runBlock.$inject =
        ['routerHelper', '$rootScope', '$state'];

    /**
     * Router initialization.
     */
    function runBlock(routerHelper, $rootScope, $state) {
        routerHelper.configureStates(getStates(), getAliases());
    }

    /**
     * The default state routes.
     */
    function getStates() {
        return [
            {
                // Note: for more advanced home pages, break into a new module
                state: 'home',
                config: {
                    templateUrl: 'core/home/index.html',
                    url: '/',
                    controller: 'HomeController',
                    controllerAs: 'vm'
                }
            },
        ];
    }

    /**
     * The aliases for existing states.
     */
    function getAliases() {
        return [
            ['', '/']
        ];
    }
})();
