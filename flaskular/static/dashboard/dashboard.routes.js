/**
 * Routes for the dashboard page.
 */
(function() {
    'use strict';

    angular
        .module('flaskular.dashboard')
        .run(runBlock);

    runBlock.$inject =
        ['routerHelper', '$rootScope', '$state'];

    /**
     * Router initialization.
     */
    function runBlock(routerHelper, $rootScope, $state) {
        routerHelper.configureStates(getStates());
    }

    /**
     * The default state routes.
     */
    function getStates() {
        return [
            {
                // Note: for more advance home pages, break into a new module
                state: 'dashboard',
                config: {
                    templateUrl: 'core/dashboard/dashboard.html',
                    url: '/dashboard',
                    // controller: 'HomeController',
                    // controllerAs: 'vm',
                    loginRequired: true
                }
            },
        ];
    }
})();
