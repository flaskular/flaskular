/**
 * Directive for defining the top navigation. Note, you will probably want
 * to make an extended topnav directive, possibly called `topnav-ext
 *
 * Usage is <topnav></topnav>
 *
 * @namespace flaskular.layout.topnav
 */
(function(){
    'use strict';

    angular
        .module('flaskular.layout.topnav')
        .controller('TopnavController', TopnavController)  // Expose to tests
        .directive('topnav', topnav);

    TopnavController.$inject =
        ['$scope', '$state', 'authService', 'topnavService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * topnav directive definition.
     */
    function topnav(authService) {
        return {
            restrict: 'E',
            templateUrl: 'core/layout/topnav.directive.html',
            controller: TopnavController,
            controllerAs: 'vm',
            bindToController: true
        };
    }

    /**
     * topnav controller definition.
     */
    function TopnavController($scope, $state, authService, topnavService) {
        var vm = this;

        // Dynamic data
        vm.currentUser = false;
        vm.isLoggedIn = false;
        vm.show = true;

        // Methods
        vm.logout = logout;
        vm.setVisibility = setVisibility;

        // Initialization
        $scope.init = initialize();

        // Extenstion
        vm.extendTo = extendTo;

        // Watches
        $scope.$watch(function() {
            return authService.isLoggedIn();
        },
        function(current, original) {
            setCurrentLoginStatus();
        });

        ///////////////////////////////////////////
        //  Implementations
        ///////////////////////////////////////////

        /**
         * Initialization of the controller.
         */
        function initialize() {
            topnavService.onTopnavToggle(setVisibility);

            setCurrentLoginStatus();
        }

        /**
         * Sets whether or not the topnav is shown.
         */
        function setVisibility(visibility) {
            vm.show = visibility;
        }

        /**
         * Sets the login status for the controller.
         */
        function setCurrentLoginStatus() {
            vm.isLoggedIn = authService.isLoggedIn();
            vm.currentUser = authService.getCurrentUser();
            vm.isAdmin = authService.hasRole('admin');
        }

        /**
         * Uses authService to log out the current user.
         */
        function logout() {
            // Handle for successful and failed logout
            function handleSuccess() {
                $state.go('home');
            }

            authService.logout()
                .then(handleSuccess)
                .catch(handleSuccess);  // Logout client no matter what.
        }

        /**
         * Make extendable.
         */
       function extendTo(child) {
           vm = angular.extend(child, vm);
           return vm;
       }

    }
})();
