/**
 * Application configuration and initialization
 */
(function(){
    'use strict';

    angular
        .module('flaskular', [
            // Load the desired modules into the app module, not here
            // (modules can be optional or extendable)

            // 'ui.router',
            // 'ui.bootstrap',
            // 'ui.checkbox',
            // 'flaskular.router',
            // 'flaskular.layout',
            // 'flaskular.home',
            // 'flaskular.dashboard',
            // 'flaskular.auth',
            // 'flaskular.admin',
            // 'flaskular.profile'
        ]);
})();
