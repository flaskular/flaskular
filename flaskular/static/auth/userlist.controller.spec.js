(function() {
    'use strict';

    describe('UserlistController', function() {

        var vm;
        var $provide;
        var stateStub;
        var authServiceStub;
        var uibModalStub;
        var q;
        var testScope;
        var currState;
        var controller;
        var userlist;


        beforeEach(module('flaskular.auth'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $controller, $q) {

            q = $q;
            testScope = $rootScope.$new();

            currState = 'auth/userlist';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                }),
            };

            authServiceStub = {
                getUserList: sinon.stub(),
                getCurrentUser: sinon.stub(),
                deleteUser: sinon.stub(),
                promoteUser: sinon.stub(),
                demoteUser: sinon.stub()
            };

            uibModalStub = {
                open: sinon.stub()
            };

            controller = $controller;

            userlist = [
                {
                    email: 'user1',
                    roles: ['admin']
                },
                {
                    email: 'user2',
                    roles: ['admin', 'subscriber']
                }
            ];

        }));

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Runs an attempt to fetch the userlist.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {list of object} userlist The expected userlist returned by
         * a successful operation.
         * @param {bool} success True if the authService resolves, false if
         * it rejects.
         */
        function runGetUserList(code, msg, userlist, success) {
            var expected = {
                code: code,
                msg: msg,
                userlist: userlist
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub.getUserList
                .returns(defer.promise);

            vm = controller('UserListController', {
                '$state': stateStub,
                'authService': authServiceStub
            });

            testScope.$apply();
        }

        /**
         * Sets up and runs a backend connection (promote, demote, delete).
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {fcn} str The backend function to run (one of 'promoteUser',
         * 'demoteUser' or 'deleteUser').
         * @param {id} int The user id.
         * @param {email} str The user's email.
         * @param {bool} success True if the authService resolves, false if it
         * rejects.
         */
        function runBackend(code, msg, fcn, id, email, success) {
            // Function backend
            var expected = {
                code: code,
                msg: msg
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub[fcn].withArgs(id)
                .returns(defer.promise);
            authServiceStub[fcn]
                .throws();

            // Userlist backend
            var listDefer = q.defer();
            listDefer.resolve({code: 'user_list_success', msg: 'Success'});
            authServiceStub.getUserList.returns(listDefer.promise);

            // Define vm
            vm = controller('UserListController', {
                '$state': stateStub,
                'authService': authServiceStub
            });

            // Run function
            vm[fcn](id, email);
            testScope.$apply();

            // Check results of function
            expect(vm.promoting[id]).not.toBe(true);
            expect(vm.deleting[id]).not.toBe(true);

            // Check call of getUserList - called once on controller init,
            // called a second time on a successfull query only
            expect(authServiceStub.getUserList.called).toBe(true);
            expect(authServiceStub.getUserList.calledTwice).toBe(success);

        }

        /**
         *  Runs the delete user modal. Assumes all backend connections
         *  succeeed.
         *
         *  @param {int} id The id of the user to delete.
         *  @param {str} email The email of the user to delete.
         *  @param {bool} success Whether the delete user is accepted by the
         *  modal.
         */
        function runDeleteModal(id, email, success) {
            // Delete backend
            var deleteDefer = q.defer();
            deleteDefer.resolve({code: 'delete_success', msg: 'Success'});
            authServiceStub.deleteUser.withArgs(id)
                .returns(deleteDefer.promise);
            authServiceStub.deleteUser
                .throws();

            // Userlist backend
            var listDefer = q.defer();
            listDefer.resolve({code: 'user_list_success', msg: 'Success'});
            authServiceStub.getUserList.returns(listDefer.promise);

            // Modal definition
            var successFcn;
            var errorFcn;
            var modalInstance = {result: {}};
            modalInstance.result.then = function(success, error) {
                successFcn = success;
                errorFcn = error;
            };
            uibModalStub.open = function(params) {
                expect(params.resolve.email()).toBe(email);
                return modalInstance;
            };

            // Define vm
            vm = controller('UserListController', {
                '$state': stateStub,
                'authService': authServiceStub,
                '$uibModal': uibModalStub
            });

            // Run function
            vm.openDeleteUser(id, email);
            testScope.$apply();
            expect(successFcn).not.toBe(undefined);
            expect(errorFcn).not.toBe(undefined);

            if (success) {
                successFcn('Close');
            }
            else {
                errorFcn();
            }
            testScope.$apply();

            // Check results of function
            expect(vm.promoting[id]).not.toBe(true);
            expect(vm.deleting[id]).not.toBe(true);

            // Check call to deleteUser, should be once if success, 0
            // otherwise.
            expect(authServiceStub.deleteUser.called).toBe(success);

            // Check call of getUserList - called once on controller init,
            // called a second time on a successfull query only
            expect(authServiceStub.getUserList.called).toBe(true);
            expect(authServiceStub.getUserList.calledTwice).toBe(success);
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        describe('initialize', function() {

            it('should successfully populate the list', function() {
                runGetUserList('user_list_success', 'Success', userlist, true);
                expect(vm.userlist).toEqual(userlist);
                expect(currState).toEqual('auth/userlist');
            });

            it('should fail (auth error)', function() {
                runGetUserList('request_role_required', 'need auth', userlist,
                               false);
                expect(vm.userlist).toEqual([]);
                expect(currState).toEqual('error');
            });
        });

        describe('openDeleteUser', function() {

            it('should successfully open the modal then delete the user',
               function() {
                runDeleteModal(42, 'example', true);
                expect(currState).toEqual('auth/userlist');
            });

            it('should dismiss the modal without doing anything', function() {
                runDeleteModal(42, 'example', false);
                expect(currState).toEqual('auth/userlist');
            });

        });

        describe('deleteUser', function() {

            it('should successfully delete a user', function() {
                runBackend('delete_success', 'Delete successful',
                           'deleteUser', 42, 'example', true);
                expect(currState).toEqual('auth/userlist');
            });

            it('should fail to delete a user', function() {
                runBackend('unknown', 'Delete failed', 'deleteUser',
                           42, 'example', false);
                expect(currState).toEqual('error');
            });

        });

        describe('promoteUser', function() {

            it('should successfully promote a user', function() {
                runBackend('promote_success', 'Promote successful',
                           'promoteUser', 42, 'example', true);
                expect(currState).toEqual('auth/userlist');
            });

            it('should fail to promote a user', function() {
                runBackend('unknown', 'Promote failed', 'promoteUser',
                           42, 'example', false);
                expect(currState).toEqual('error');
            });

        });

        describe('demoteUser', function() {

            it('should successfully demote a user', function() {
                runBackend('demote_success', 'Demote successful', 'demoteUser',
                           42, 'example', true);
                expect(currState).toEqual('auth/userlist');
            });

            it('should fail to demote a user', function() {
                runBackend('unknown', 'Demote failed', 'demoteUser',
                           42, 'example', false);
                expect(currState).toEqual('error');
            });

        });
    });
})();
