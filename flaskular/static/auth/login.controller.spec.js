(function() {
    'use strict';

    describe('LoginController', function() {

        var vm;
        var $provide;
        var stateStub;
        var authServiceStub;
        var q;
        var testScope;
        var currState;

        beforeEach(module('flaskular.auth'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function($rootScope, $controller, $q) {

            q = $q;
            testScope = $rootScope.$new();

            currState = 'auth/login';
            stateStub = {
                go: sinon.spy(function(arg) {
                    currState = arg;
                }),
            };

            authServiceStub = {
                login: sinon.stub(),
                logout: sinon.stub(),
                isLoggedIn: sinon.stub()
            };

            vm = $controller('LoginController', {
                '$state': stateStub,
                'authService': authServiceStub
            });
        }));

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Runs a login attempt.
         *
         * @param {str} code The code returned by the authService.
         * @param {str} msg The message returned by the authService.
         * @param {str} email The email to place into the login form.
         * @param {str} password The password to place into the login form.
         * @param {bool} success True if the authService resolves, false if
         * it rejects.
         */
        function runLogin(code, msg, email, password, success) {
            var expected = {
                code: code,
                msg: msg
            };
            var defer = q.defer();

            if (success) {
                defer.resolve(expected);
            }
            else {
                defer.reject(expected);
            }

            authServiceStub.login.withArgs(email, password)
                .returns(defer.promise);
            authServiceStub.login
                .throws();

            vm.loginForm = {
                email: email,
                password: password
            };

            vm.login();
            testScope.$apply();

            expect(vm.disabled).toBe(false);
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //-------------
        //  Login Tests
        //-------------

        describe('vm.login', function() {

            it('should log in a user', function() {
                runLogin('login_success', 'Login successful',
                         'real@example.com', 'real', true);

                expect(vm.loginForm).toEqual({});
                expect(currState).toEqual('home');
            });

            it('should not login due to a bad email', function() {
                runLogin('login_unknown_user', 'Unknown user: <user>',
                         'fake@example.com', 'fake', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.error).toBe(true);
                expect(vm.errorMessage).toEqual('Unknown user: <user>');

                expect(currState).toEqual('auth/login');
            });

            it('should not login due to a bad password', function() {
                runLogin('login_invalid_password', 'Bad password',
                         'real@example.com', 'bad'. false);

                expect(vm.loginForm).toEqual({
                    email: 'real@example.com'
                });
                expect(vm.error).toBe(true);
                expect(vm.errorMessage).toEqual('Bad password');

                expect(currState).toEqual('auth/login');
            });

            it('should not login since somebody is already logged in',
               function() {
                runLogin('login_dual', 'Already logged in',
                         'dual@example.com', 'whatever', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.error).toBe(true);
                expect(vm.errorMessage).toEqual('Already logged in');

                expect(currState).toEqual('error');
            });

            it('should create a client error due to missing data', function() {
                runLogin('', '', '', '', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.error).toBe(true);
                expect(vm.errorMessage)
                    .toBe('An unknown client-side error has occured');

                expect(currState).toEqual('error');
            });

            it('should also create a client error', function() {
                runLogin(undefined, undefined, '', '', false);

                expect(vm.loginForm).toEqual({});
                expect(vm.error).toBe(true);
                expect(vm.errorMessage)
                    .toBe('An unknown client-side error has occured');

                expect(currState).toEqual('error');
            });

        });

    });
})();

