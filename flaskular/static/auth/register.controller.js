/**
 * The registration controller.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .controller('RegisterController', RegisterController);

    RegisterController.$inject = ['$state', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Register controller definition.
     */
    function RegisterController($state, authService) {
        var vm = this;

        vm.register = register;

        //////////////////////////////////////////
        //  Impmentations
        //////////////////////////////////////////

        /**
         * Attempts to register a new user using information in the
         * registration form.
         */
        function register() {
            vm.error = false;
            vm.disabled = true;

            // Handle for a successful registration
            function handleSuccess(data) {
                $state.go('auth/login'); // TODO add data
                vm.disabled = false;
                vm.registerForm = {};
            }

            // Handle for a failed registration
            function handleError(data) {
                vm.error = true;
                vm.errorMessage = data.msg;
                if (vm.errorMessage === undefined ||
                        vm.errorMessage.trim() === '') {
                    // Make sure some data comes in.
                    data.code = 'unknown';
                    data.msg = 'An unknown client-side error has occured';
                    vm.errorMessage = data.msg;
                }

                vm.disabled = false;
                vm.registerForm = {};

                if (data.code !== 'register_already_exists') {
                    $state.go('error'); // TODO add data
                }
            }

            // Make the registration attempt
            authService.register(vm.registerForm.email,
                                 vm.registerForm.password)
                .then(handleSuccess)
                .catch(handleError);
        }
    }
})();
