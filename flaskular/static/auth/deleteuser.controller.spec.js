(function() {
    'use strict';

    describe('DeleteUserController', function() {
        var vm;
        var uibModalInstanceStub;
        var email = 'test@example.com';

        beforeEach(module('flaskular.auth'));

        beforeEach(inject(function($controller) {

            uibModalInstanceStub = {
                close: sinon.stub(),
                dismiss: sinon.stub()
            };

            vm = $controller('DeleteUserController', {
                '$uibModalInstance': uibModalInstanceStub,
                'email': email
            });

        }));

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //----------------
        //  Initialization
        //----------------

        it('should be initialized with the proper email', function() {
            expect(vm.email).toEqual(email);
        });

        //-----------
        //  Functions
        //-----------

        it('should close the modal', function() {
            vm.ok();
            expect(uibModalInstanceStub.close.called).toBe(true);
        });

        it('should dismiss the modal', function() {
            vm.cancel();
            expect(uibModalInstanceStub.dismiss.called).toBe(true);
        });

    });
})();
