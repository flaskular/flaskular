/**
 * Routes for the auth module.
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .run(appRun);

    /* @ngInject */
    function appRun(routerHelper) {
        routerHelper.configureStates(getStates());
    }

    /**
     * Gets the route states.
     */
    function getStates() {
        return [
            {
                state: 'auth/login',
                config: {
                    'templateUrl': 'core/auth/login.html',
                    'url': '/login',
                    'controller': 'LoginController',
                    'controllerAs': 'vm'
                }
            },
            {
                state: 'auth/register',
                config: {
                    'templateUrl': 'core/auth/register.html',
                    'url': '/register',
                    'controller': 'RegisterController',
                    'controllerAs': 'vm'
                }
            },
            {
                state: 'auth/userlist',
                config: {
                    'templateUrl': 'core/auth/userlist.html',
                    'url': '/auth/admin/userlist',
                    'controller': 'UserListController',
                    'controllerAs': 'vm'
                }
            }
        ];
    }
})();
