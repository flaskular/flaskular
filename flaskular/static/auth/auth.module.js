/**
 * Module definition for authentication.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth', [
            'ngStorage',
            'ngCookies',
            'ui.bootstrap',
            'ui.router',
            'flaskular.router'
        ]);
})();
