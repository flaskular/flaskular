/**
 * The controller for the modal to confirm the deletion of a user.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .controller('DeleteUserController', DeleteUserController);

    DeleteUserController.$inject = ['$uibModalInstance', 'email'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * Delete User controller definition
     */
    function DeleteUserController($uibModalInstance, email) {
        var ml = this;

        // Data
        ml.email = email;

        // Methods
        ml.ok = ok;
        ml.cancel = cancel;

        ////////////////////////////////////////
        //  Implementations
        ////////////////////////////////////////

        /**
         * Triggered when the "Yes" button is clicked.
         */
        function ok() {
            $uibModalInstance.close({});
        }

        /**
         * Triggered when the "No" button is clicked.
         */
        function cancel() {
            $uibModalInstance.dismiss('cancel');
        }
    }
})();
