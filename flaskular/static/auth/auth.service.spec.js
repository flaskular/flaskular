(function() {
    'use strict';

    describe('authService Tests', function() {

        var authService;
        var q;
        var timeoutStub;
        var httpBackend;
        var cookies;

        beforeEach(module('flaskular.auth'));

        beforeEach(module(function($urlRouterProvider) {
            $urlRouterProvider.deferIntercept();
        }));

        beforeEach(inject(function(_authService_, $httpBackend,
                                   $cookies) {
            authService = _authService_;
            httpBackend = $httpBackend;
            cookies = $cookies;
        }));

        afterEach(function() {
            httpBackend.verifyNoOutstandingExpectation();
            httpBackend.verifyNoOutstandingRequest();
        });

        ///////////////////////////////////////////////////////////////////////
        //  Helpers
        ///////////////////////////////////////////////////////////////////////

        /**
         * Tests the promise from the authService on the httpBackend configured
         * for the test.
         *
         * @param {promise} promise The promise to run.
         * @param {boolean} shouldSucceed True if the promise should succeed
         * (tested), which should also cause the service to send back data with
         * a true success code (also tested). False if the promise fail with
         * an error (tested), which should also cause the service to send
         * back data with a  false success code (also tested).
         * @param {str} expectedCode The code that the test case expects the
         * service to send back in data.
         *
         * @return {object} result The response data sent back by the
         * service. Use to test additional properties of this object, though
         * the 'code' and 'success' properties are tested in this helper.
         */
        function testAuthServicePromise(promise, shouldSucceed, expectedCode) {
            var shouldntCall = sinon.stub();

            var result;
            promise.then(function(response) {
                // Run on promise success
                if (!shouldSucceed) {
                    shouldntCall();
                    console.log('Unexpected success with response data:');
                    console.log(response);
                }
                result = response;
            })
            .catch(function(response) {
                // Run on promise error
                if (shouldSucceed) {
                    shouldntCall();
                    console.log('Unexpected error with response data:');
                    console.log(response);
                }
                result = response;
            });

            httpBackend.flush();

            expect(shouldntCall).not.toHaveBeenCalled();
            expect(result).not.toBe(undefined);
            expect(result.success).toBe(shouldSucceed);
            expect(result.code).toEqual(expectedCode);

            return result;
        }

        /**
        * Wraps a response data in order to return a status code too.
        */
        function response(data) {
            return [200, data, {}];
        }

        ///////////////////////////////////////////////////////////////////////
        //  Tests
        ///////////////////////////////////////////////////////////////////////

        //------------------
        //  authService POST
        //------------------

        describe('post', function() {

            var shouldSucceed;
            var succeeded;

            var testSuccess;
            var testError;

            var data;

            beforeEach(function() {

                testSuccess = sinon.stub();
                testError = sinon.stub();

                httpBackend
                    .whenPOST('test')
                    .respond(function(method, url, json) {
                        data = angular.fromJson(json);

                        expect(data.token).toBe('t1');

                        if (shouldSucceed) {
                            succeeded = true;
                            return response({
                                code: 'success',
                                success: true,
                                data: data
                            });
                        }
                        else {
                            succeeded = false;
                            return response({
                                code: 'fail',
                                success: false,
                                data: data
                            });
                        }
                    });
                httpBackend
                    .whenPOST('/auth/login')
                    .respond(function(method, url, json) {
                        return response({
                            code: 'success',
                            success: true,
                            user: {
                                id: 42,
                                email: 'example'
                            },
                            token: 't1'
                        });
                    });

                authService.login();
                httpBackend.flush();
            });

            it ('should run with no data and given callbacks', function() {
                shouldSucceed = true;
                var promise = authService.post('test');
                promise.then(testSuccess).catch(testError);

                httpBackend.flush();
                expect(testSuccess.called).toBe(true);
                expect(testError.called).toBe(false);
            });

            it ('should fail to run with no data and given callbacks',
                function() {
                shouldSucceed = false;

                var promise = authService.post('test');
                promise.then(testSuccess).catch(testError);

                httpBackend.flush();
                expect(testSuccess.called).toBe(false);
                expect(testError.called).toBe(true);
            });

            it ('should run with data and given callbacks', function() {
                shouldSucceed = true;
                var promise = authService.post('test', {a: 'b'});
                promise.then(testSuccess).catch(testError);

                httpBackend.flush();
                expect(testSuccess.called).toBe(true);
                expect(testError.called).toBe(false);
            });

            it ('should fail to run with data and given callbacks',
                function() {

                shouldSucceed = false;

                var promise = authService.post('test', {a: 'b'});
                promise.then(testSuccess).catch(testError);

                httpBackend.flush();
                expect(testSuccess.called).toBe(false);
                expect(testError.called).toBe(true);
            });

        });

        //-------------
        //  Roles Tests
        //-------------

        describe('roles', function() {

            beforeEach(function() {

                httpBackend
                    .whenPOST('/auth/login')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);
                        var email = data.email;
                        var roles;

                        if (email === 'admin') {
                            roles = ['admin'];
                        }
                        if (email === 'subscriber') {
                            roles = ['subscriber'];
                        }
                        if (email === 'both') {
                            roles = ['admin', 'subscriber'];
                        }

                        return response({
                            code: 'success',
                            success: true,
                            user: {
                                email: 'test@example.com',
                                roles: roles
                            },
                            token: 'token'
                        });

                    });
            });

            it('should have the role', function() {
                var promise = authService.login('admin', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasRole('admin')).toBe(true);
            });

            it('should have the role among multiple', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasRole('admin')).toBe(true);
            });

            it('should not have the role', function() {
                var promise = authService.login('subscriber', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasRole('admin')).toBe(false);
            });

            it('should not have the role among multiple', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasRole('other')).toBe(false);
            });

            it('should have all of undefined', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasAllRoles()).toBe(true);
            });

            it('should have all of false', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasAllRoles(false)).toBe(true);
            });

            it('should have all of one', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasAllRoles(['admin'])).toBe(true);
            });

            it('should have all of multiple', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasAllRoles(['admin', 'subscriber']))
                    .toBe(true);
            });

            it('should not have all', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasAllRoles(['admin', 'subscriber',
                                                'other']))
                    .toBe(false);
            });

            it('should have one of undefined', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole()).toBe(true);
            });

            it('should have one of false', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole(false)).toBe(true);
            });

            it('should have one of one', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole(['admin'])).toBe(true);
            });

            it('should have one of multiple', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole(['admin', 'subscriber']))
                    .toBe(true);
            });

            it('should have one even missing one', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole(['admin', 'subscriber',
                                               'other']))
                    .toBe(true);
            });

            it('should not have one', function() {
                var promise = authService.login('both', '');
                testAuthServicePromise(promise, true, 'success');

                expect(authService.hasOneRole(['other'])).toBe(false);
            });
        });

        //-------------
        //  Login Tests
        //-------------

        describe('login', function() {
            beforeEach(function() {

                httpBackend
                    .whenPOST('/auth/login')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);
                        var email = data.email;
                        var password = data.password;

                        // Correct email and password
                        if (email === 'test@example.com' &&
                                password === 'test') {
                            return response({
                                code: 'login_success',
                                success: true,
                                msg: 'Success!',
                                user: {
                                    email: 'test@example.com',
                                    roles: ['admin']
                                },
                                token: '12345'
                            });
                        }

                        // Dual login
                        if (email === 'dual@example.com' &&
                                password === 'dual') {
                            return response({
                                code: 'login_dual',
                                success: false,
                                msg: 'Dual login'
                            });
                        }

                        // Wrong email
                        if (email === 'bogus@example.com') {
                            return response({
                                code: 'login_unknown_user',
                                success: false,
                                msg: 'Unknown user'
                            });
                        }

                        // Correct email, wrong password
                        if (email === 'test@example.com' &&
                            password !== 'test') {

                            return response({
                                code: 'login_invalid_password',
                                success: false,
                                msg: 'Wrong password'
                            });
                        }

                        // Dead server
                        if (email === 'killserver@example.com') {
                            return [304, {}, {}];
                        }

                        // Misc
                        return response({
                            code: 'unknown',
                            success: false,
                            msg: 'Something went wrong'
                        });
                    });
            });

            it('should perform a successful login', function() {
                var promise = authService.login('test@example.com', 'test');
                testAuthServicePromise(promise, true, 'login_success');

                expect(authService.isLoggedIn()).toEqual(true);
                expect(authService.getCurrentUser().email)
                    .toEqual('test@example.com');
                expect(cookies.getObject('token')).toEqual('12345');
                expect(authService.hasRole('admin')).toBe(true);
                expect(authService.hasRole('other')).toBe(false);
            });

            it('should give an error about a dual login', function() {
                var promise = authService.login('dual@example.com', 'dual');
                testAuthServicePromise(promise, false, 'login_dual');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
                expect(authService.hasRole('admin')).toBe(false);
            });


            it('should give an error about an unknown user', function() {
                var promise = authService.login('bogus@example.com', 'test');
                testAuthServicePromise(promise, false, 'login_unknown_user');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should give an error about an invalid password', function() {
                var promise = authService.login('test@example.com', 'bogus');
                testAuthServicePromise(promise, false,
                                       'login_invalid_password');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should kill the server', function() {
                var promise = authService.login(
                    'killserver@example.com', 'bogus');
                testAuthServicePromise(promise, false,
                                       'critical_server_error');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should create an unknown error', function() {
                var promise = authService.login('unknown', 'bogus');
                testAuthServicePromise(promise, false, 'unknown');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });
        });

        //-------------
        //  Token Login
        //-------------

        describe('tokenLogin', function() {
            beforeEach(function() {

                httpBackend
                    .whenPOST('/auth/token-login')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);
                        var token = data.token;

                        // Success
                        if (token === 'success') {
                            return response({
                                code: 'token_login_success',
                                success: true,
                                msg: 'Success!',
                                user: {
                                    email: 'token@example.com',
                                    roles: ['admin']
                                },
                                token: 'success'
                            });
                        }

                        // Signature expired
                        if (token === 'expired') {
                            return response({
                                code: 'token_login_expired',
                                success: false
                            });
                        }

                        // Bad signature
                        if (token === 'bad') {
                            return response({
                                code: 'token_login_invalid',
                                success: false
                            });
                        }

                        // Logged out
                        if (token === 'logged_out') {
                            return response({
                                code: 'token_login_logged_out',
                                success: false
                            });
                        }

                        // Dead server
                        if (token === 'killserver') {
                            return [304, {}, {}];
                        }

                        // Misc
                        return response({
                            code: 'unknown',
                            success: false
                        });
                    });
            });

            it('should perform a successful token login', function() {
                var promise = authService.tokenLogin('success');
                testAuthServicePromise(promise, true, 'token_login_success');

                expect(authService.isLoggedIn()).toEqual(true);
                expect(authService.getCurrentUser().email)
                    .toEqual('token@example.com');
                expect(cookies.getObject('token')).toEqual('success');
                expect(authService.hasRole('admin')).toBe(true);
                expect(authService.hasRole('other')).toBe(false);
            });

            it('should fail due to an expired token', function() {
                var promise = authService.tokenLogin('expired');
                testAuthServicePromise(promise, false, 'token_login_expired');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
                expect(authService.hasRole('admin')).toBe(false);
            });

            it('should fail due to a bad token', function() {
                var promise = authService.tokenLogin('bad');
                testAuthServicePromise(promise, false, 'token_login_invalid');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should fail due to the user being logged out', function() {
                var promise = authService.tokenLogin('logged_out');
                testAuthServicePromise(promise, false,
                                       'token_login_logged_out');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should fail due to a dead server', function() {
                var promise = authService.tokenLogin('killserver');
                testAuthServicePromise(promise, false,
                                       'critical_server_error');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });

            it('should fail due an unknown error', function() {
                var promise = authService.tokenLogin('bogus');
                testAuthServicePromise(promise, false, 'unknown');

                expect(authService.isLoggedIn()).toEqual(false);
                expect(authService.getCurrentUser()).toEqual(false);
                expect(cookies.getObject('token')).toBe(undefined);
            });
        });

        //----------------------
        //  loginIfCurrent Tests
        //----------------------

        describe('loginIfCurrent', function() {
            var loggedIn = false;

            beforeEach(function() {
                httpBackend
                    .whenGET('/auth/current-user')
                    .respond(function(method, url, json) {

                        if (loggedIn) {
                            return response({
                                code: 'current_user_success',
                                success: true,
                                msg: 'Success!',
                                user: {
                                    email: 'example@example.com',
                                    roles: ['subscriber']
                                },
                                token: 'success',
                            });
                        }
                        else {
                            return response({
                                code: 'current_user_fail',
                                success: false
                            });
                        }
                    });
                httpBackend
                    .whenPOST('/auth/token-login')
                    .respond(function(method, url, json) {

                        if (loggedIn) {
                            return response({
                                code: 'token_login_success',
                                success: true,
                                msg: 'Success!',
                                user: {
                                    email: 'example@example.com',
                                    roles: ['subscriber']
                                },
                                token: 'success',
                            });
                        }
                        else {
                            return response({
                                code: 'token_login_fail',
                                success: false
                            });
                        }
                });
            });

            it('should not login', function() {
                loggedIn = false;
                authService.loginIfCurrent();
                httpBackend.flush();

                expect(authService.isLoggedIn()).toBe(false);
                expect(authService.getCurrentUser()).toBe(false);
                expect(cookies.getObject('token')).toBe(undefined);
                expect(authService.hasRole('subscriber')).toBe(false);
            });

            it('should login', function() {
                loggedIn = true;
                authService.loginIfCurrent();
                httpBackend.flush();

                expect(authService.isLoggedIn()).toBe(true);
                expect(authService.getCurrentUser().email)
                    .toBe('example@example.com');
                expect(cookies.getObject('token')).toBe('success');
                expect(authService.hasRole('subscriber')).toBe(true);
                expect(authService.hasRole('admin')).toBe(false);
            });
        });
        //--------
        //  Logout
        //---------

        describe('logout', function() {
            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/login')
                    .respond(function(method, url, json) {
                        return response({
                            code: 'login_success',
                            success: true,
                            msg: 'success!',
                            user: {
                                email: 'email',
                                token: 'token',
                                roles: ['admin']
                            }
                        });
                    });
                httpBackend
                    .whenGET('/auth/logout')
                    .respond(function(method, url, json) {

                        if (authService.isLoggedIn()) {
                            return response({
                                code: 'logout_success',
                                success: true,
                                msg: 'Logout successful'
                            });
                        }
                        else {
                            return response({
                                code: 'logout_nobody',
                                success: false,
                                msg: 'Nobody to log out'
                            });
                        }

                    });
            });

            it('should successfully log out', function() {
                var inpromise = authService.login('test@example.com', 'test');
                testAuthServicePromise(inpromise, true, 'login_success');

                expect(authService.isLoggedIn()).toBe(true);
                expect(authService.hasRole('admin')).toBe(true);

                var promise = authService.logout();
                testAuthServicePromise(promise, true, 'logout_success');

                expect(authService.isLoggedIn()).toBe(false);
                expect(authService.getCurrentUser()).toBe(false);
                expect(cookies.getObject('token')).toBe(undefined);
                expect(authService.hasRole('admin')).toBe(false);
            });

            it('should fail to log out', function() {
                var promise = authService.logout();
                testAuthServicePromise(promise, false, 'logout_nobody');

                expect(authService.isLoggedIn()).toBe(false);
                expect(authService.getCurrentUser()).toBe(false);
                expect(cookies.getObject('token')).toBe(undefined);
                expect(authService.hasRole('admin')).toBe(false);
            });
        });

        //----------
        //  Register
        //----------

        describe('register', function() {
            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/register')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);
                        var email = data.email;

                        if (email === 'existing@example.com'){
                            return response({
                                code: 'register_already_exists',
                                success: false,
                                msg: 'User "existing@example.com" exists.'
                            });
                        }
                        else {
                            return response({
                                code: 'register_success',
                                success: true,
                                msg: 'Registration successful'
                            });
                        }
                    });
            });

            it('should register a new user', function() {
                var promise = authService.register('new@example.com', 'new');
                testAuthServicePromise(promise, true, 'register_success');
            });

            it('should fail to register a duplicate user', function() {
                var promise = authService.register('existing@example.com',
                                                   '2');
                testAuthServicePromise(promise, false,
                                       'register_already_exists');
            });
        });

        //---------------
        //  Get User List
        //---------------

        describe('getUserList', function() {
            var authorized;
            var expected = [
                {
                    'email': 'user1',
                    'roles': ['admin']
                },
                {
                    'email': 'user2',
                    'roles': ['subscriber']
                }
            ];

            beforeEach(function() {
                authorized = false;

                httpBackend
                    .whenPOST('/auth/user-list')
                    .respond(function(method, url, json) {

                        if (authorized) {
                            return response({
                                code: 'user_list_success',
                                success: true,
                                userlist: expected
                            });
                        }

                        else {
                            return response({
                                code: 'request_role_required',
                                success: false
                            });
                        }

                    });
            });

            it('should return the list', function() {
                authorized = true;
                var promise = authService.getUserList();
                var rs = testAuthServicePromise(
                    promise, true, 'user_list_success');

                expect(rs.userlist).not.toBe(undefined);
                expect(rs.userlist).toEqual(expected);
            });

            it('should fail since user is not authenticated', function() {
                var promise = authService.getUserList();
                var rs = testAuthServicePromise(
                    promise, false, 'request_role_required');

                expect(rs.userlist).toBe(undefined);
            });

        });

        //---------------
        //  Get User Dict
        //---------------

        describe('getUserDict', function() {
            var loggedIn;

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/user-dict')
                    .respond(function(method, url, json) {
                        if (loggedIn) {
                            return response({
                                code: 'get_user_dict_success',
                                success: true,
                                user: {
                                    id: 42,
                                    email: 'example'
                                }
                            });
                        }

                        else {
                            return response({
                                code: 'unknown',
                                success: false,
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                loggedIn = true;
                var promise = authService.getUserDict();
                var rs = testAuthServicePromise(
                    promise, true, 'get_user_dict_success'
                );
                expect(rs.user).not.toBe(undefined);
                expect(rs.user.id).toBe(42);
            });

            it('should fail', function() {
                loggedIn = false;
                var promise = authService.getUserDict();
                var rs = testAuthServicePromise(
                    promise, false, 'unknown'
                );
                expect(rs.user).toBe(undefined);
            });

        });

        //-----------------
        //  Change Password
        //-----------------

        describe('changePassword', function() {

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/change-password')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);

                        if (data.id === 1 && data.oldPassword !== undefined &&
                            data.newPassword !== undefined) {
                            return response({
                                code: 'password_change_success',
                                success: true
                            });
                        }

                        else {
                            return response({
                                code: 'password_change_incorrect_old',
                                success: false
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                var promise = authService.changePassword(1, 'a', 'b');
                testAuthServicePromise(
                    promise, true, 'password_change_success'
                );
            });

            it('should fail', function() {
                var promise = authService.changePassword(42, 'a', 'b');
                testAuthServicePromise(
                    promise, false, 'password_change_incorrect_old'
                );
            });

            it('should fail too', function() {
                var promise = authService.changePassword(42);
                testAuthServicePromise(
                    promise, false, 'password_change_incorrect_old'
                );
            });

        });

        //----------------
        //  Update Profile
        //----------------

        describe('updateProfile', function() {

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/update-profile')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);

                        if (data.id === 1 && data.firstName !== undefined &&
                            data.lastName !== undefined) {
                            return response({
                                code: 'update_profile_success',
                                success: true
                            });
                        }

                        else {
                            return response({
                                code: 'update_profile_not_authorized',
                                success: false
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                var promise = authService.updateProfile(1, 'a', 'b');
                testAuthServicePromise(
                    promise, true, 'update_profile_success'
                );
            });

            it('should fail', function() {
                var promise = authService.updateProfile(42, 'a', 'b');
                testAuthServicePromise(
                    promise, false, 'update_profile_not_authorized'
                );
            });

            it('should fail too', function() {
                var promise = authService.updateProfile(42);
                testAuthServicePromise(
                    promise, false, 'update_profile_not_authorized'
                );
            });

        });

        //-------------
        //  Delete User
        //-------------

        describe('deleteUser', function() {

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/delete-user')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);

                        if (data.id === 1) {
                            return response({
                                code: 'delete_success',
                                success: true
                            });
                        }

                        else {
                            return response({
                                code: 'unknown',
                                success: false
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                var promise = authService.deleteUser(1);
                testAuthServicePromise(
                    promise, true, 'delete_success'
                );
            });

            it('should fail', function() {
                var promise = authService.deleteUser(42);
                testAuthServicePromise(
                    promise, false, 'unknown'
                );
            });

        });

        //--------------
        //  Promote User
        //--------------

        describe('promoteUser', function() {

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/promote-user')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);

                        if (data.id === 1) {
                            return response({
                                code: 'promote_success',
                                success: true
                            });
                        }

                        else {
                            return response({
                                code: 'unknown',
                                success: false
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                var promise = authService.promoteUser(1);
                testAuthServicePromise(
                    promise, true, 'promote_success'
                );
            });

            it('should fail', function() {
                var promise = authService.promoteUser(42);
                testAuthServicePromise(
                    promise, false, 'unknown'
                );
            });

        });

        //-------------
        //  Demote User
        //-------------

        describe('demoteUser', function() {

            beforeEach(function() {
                httpBackend
                    .whenPOST('/auth/demote-user')
                    .respond(function(method, url, json) {
                        var data = angular.fromJson(json);

                        if (data.id === 1) {
                            return response({
                                code: 'demote_success',
                                success: true
                            });
                        }

                        else {
                            return response({
                                code: 'unknown',
                                success: false
                            });
                        }

                    });
            });

            it ('should succeed', function() {
                var promise = authService.demoteUser(1);
                testAuthServicePromise(
                    promise, true, 'demote_success'
                );
            });

            it('should fail', function() {
                var promise = authService.demoteUser(42);
                testAuthServicePromise(
                    promise, false, 'unknown'
                );
            });

        });

    });
})();
