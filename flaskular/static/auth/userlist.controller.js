/**
 * The user list controller, used for showing admins who has registered.
 *
 * @namespace flaskular.auth
 */
(function() {
    'use strict';

    angular
        .module('flaskular.auth')
        .controller('UserListController', UserListController);

    UserListController.$inject =
        ['$state', '$timeout', '$uibModal', 'authService'];

    ///////////////////////////////////////////////////////////////////////////

    /**
     * UserList controller definition.
     */
    function UserListController($state, $timeout, $uibModal, authService) {
        var vm = this;

        // Data
        vm.userlist = [];
        vm.currUser = false;
        vm.promoting = {};
        vm.deleting = {};

        // Methods
        //  (note: delete user is exposed to tests; however, don't use it
        //   directly. It should only be called within openDeleteUser)
        vm.initialize = initialize;
        vm.openDeleteUser = openDeleteUser;
        vm.deleteUser = deleteUser;
        vm.promoteUser = promoteUser;
        vm.demoteUser = demoteUser;

        // Initialization
        vm.initialize();

        ///////////////////////////////////////
        //  Clock Tick
        ///////////////////////////////////////

        tick();

        ///////////////////////////////////////
        //  Implementations
        ///////////////////////////////////////

        /**
         * Called on controller load. Gets and populates the user list,
         * or redirects if not authenticated.
         *
         * Secure data is never recieved if not authenticated.
         */
        function initialize() {


            // handle for a successful fetch
            function handleSuccess(data) {
                vm.userlist = data.userlist;
                vm.currUser = authService.getCurrentUser();
            }

            // handle for a failed fetch
            function handleError(data) {
                vm.userlist = [];
                $state.go('error', data);
            }

            authService.getUserList()
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Ticks the clock, setting the current time.
         */
        function tick() {
            vm.clock = new Date();
            $timeout(tick, 1000);
        }

        /**
         * Opens the modal to confirm a deletion of a user. The modal then
         * executes the actual deletion.
         *
         * @param {int} id The user's id.
         * @param {str} email The user's email.
         */
        function openDeleteUser(id, email) {
            vm.deleting[id] = true;

            // Handle for close
            function handleClose(info) {
                deleteUser(id, email);
            }

            // Handle for dismissal
            function handleDismissal() {
                vm.deleting[id] = false;
            }

            // Create modal
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'core/auth/deleteuser.html',
                controller: 'DeleteUserController',
                controllerAs: 'ml',
                resolve: {
                    email: function() {
                        return email;
                    }
                }
            });

            // Run modal
            modalInstance.result.then(handleClose, handleDismissal);
        }

        /**
         * Deletes a user.
         *
         * @param {int} id The user's id.
         * @param {str} email The user's email.
         */
        function deleteUser(id, email) {
            vm.deleting[id] = true;

            // Handle for success
            function handleSuccess(data) {
                vm.deleting[id] = false;
                initialize();
            }

            // Handle for error
            function handleError(data) {
                vm.deleting[id] = false;
                $state.go('error', data, {});
            }

            authService.deleteUser(id)
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Promotes a user to an admin.
         *
         * @param {int} id The user's id.
         * @param {str} email The user's email.
         */
        function promoteUser(id, email) {
            vm.promoting[id] = true;

            // Handle for success
            function handleSuccess(data) {
                vm.promoting[id] = false;
                initialize();
            }

            // Handle for error
            function handleError(data) {
                vm.promoting[id] = false;
                $state.go('error', data, {});
            }

            authService.promoteUser(id)
                .then(handleSuccess)
                .catch(handleError);
        }

        /**
         * Demotes a user from an admin.
         *
         * @param {int} id The user's id.
         * @param {str} email The user's email.
         */
        function demoteUser(id, email) {
            vm.promoting[id] = true;

            // Handle for success
            function handleSuccess(data) {
                vm.promoting[id] = false;
                initialize();
            }

            // Handle for error
            function handleError(data) {
                vm.promoting[id] = false;
                $state.go('error', data, {});
            }

            authService.demoteUser(id)
                .then(handleSuccess)
                .catch(handleError);
        }
    }
})();
