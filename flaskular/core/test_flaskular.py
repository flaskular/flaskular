import pytest

from flaskular.test_base import TestController
from flaskular.codes import CodeManager
from flaskular.decorators import json_post


class TestFlaskular(TestController):

    def setUp(self):
        TestController.setUp(self)
        self.c = CodeManager({
            'success_code': 'A success code',
            'fail_code': 'This failed'
        })

    def test_overwrite_once(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'], overwritable=True)
        @json_post()
        def orig():
            return {
                'code': self.c.fail_code()
            }

        @app.route(url, endpoint='orig', methods=['POST'])
        @json_post()
        def overwrite():
            return {
                'code': self.c.success_code()
            }

        data, status = self.post(url)

        assert data['success']

    def test_fail_overwrite_once(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'])
        @json_post()
        def orig():
            return {
                'code': self.c.fail_code()
            }

        with pytest.raises(AssertionError):
            @app.route(url, endpoint='orig', methods=['POST'])
            @json_post()
            def overwrite():
                return {
                    'code': self.c.success_code()
                }

    def test_overwrite_twice(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'], overwritable=True)
        @json_post()
        def orig():
            return {
                'code': self.c.fail_code()
            }

        @app.route(url, endpoint='orig', methods=['POST'], overwritable=True)
        @json_post()
        def overwrite():
            return {
                'code': self.c.fail_code()
            }

        @app.route(url, endpoint='orig', methods=['POST'])
        @json_post()
        def third():
            return {
                'code': self.c.success_code()
            }

        data, status = self.post(url)

        assert data['success']

    def test_fail_overwrite_twice(self):

        app = self.app
        url = '/test'

        @app.route(url, methods=['POST'], overwritable=True)
        @json_post()
        def orig():
            return {
                'code': self.c.fail_code()
            }

        @app.route(url, endpoint='orig', methods=['POST'])
        @json_post()
        def overwrite():
            return {
                'code': self.c.fail_code()
            }

        with pytest.raises(AssertionError):
            @app.route(url, endpoint='orig', methods=['POST'])
            @json_post()
            def third():
                return {
                    'code': self.c.success_code()
                }
