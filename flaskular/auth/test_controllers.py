import time

from flask.ext.security.utils import verify_password

from flaskular.factory import db
from flaskular.test_base import TestController
from .models import FlaskularUser, FlaskularRole


class TestControllers(TestController):

    def test_login_admin(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')

        assert status == 200
        assert data['success']
        assert data['code'] == 'login_success'
        assert data['user']['email'] == 'admin@example.com'
        assert len(data['user']['roles']) == 1
        assert data['user']['roles'][0] == 'admin'

    def test_login_second(self):
        # Should work
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']

        d2, s2 = self.post('/auth/login', email='admin@example.com',
                           password='admin')

        assert s2 == 200
        assert d2['success']
        assert d2['code'] == 'login_success'
        assert d2['user']['email'] == 'admin@example.com'
        assert len(d2['user']['roles']) == 1
        assert d2['user']['roles'][0] == 'admin'

    def test_login_dual(self):
        d, s = self.post('/auth/register', email='bogus@example.com',
                         password='bogus')
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']

        d2, s2 = self.post('/auth/login', email='bogus@example.com',
                           password='bogus')
        assert s2 == 200
        assert d2['success']
        assert d2['user']['email'] == 'bogus@example.com'

    def test_login_unknown_user(self):
        data, status = self.post('/auth/login', email='bogus@example.com',
                                 password='aa')

        assert status == 200
        assert not data['success']
        assert data['code'] == 'login_unknown_user'

    def test_login_invalid_password(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='bogus')

        assert status == 200
        assert not data['success']
        assert data['code'] == 'login_invalid_password'

    def test_register(self):
        data, status = self.post('/auth/register', email='test1@example.com',
                                 password='test1')

        assert status == 200
        assert data['success']
        assert data['code'] == 'register_success'

        # Make sure new user can log in
        d2, s2 = self.post('/auth/login', email='test1@example.com',
                           password='test1')
        assert s2 == 200
        assert d2['success']
        assert d2['code'] == 'login_success'
        assert len(d2['user']['roles']) == 1
        assert d2['user']['roles'][0] == 'subscriber'

        # Make sure new user exists in db and has the right role
        user = FlaskularUser.query.filter_by(email='test1@example.com').first()

        assert user is not None
        assert user.email == 'test1@example.com'
        assert verify_password('test1', user.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert not user.has_role(admin_role)
        assert user.has_role(sub_role)

    def test_register_already_exists(self):
        # Figure out what already exists
        count = len(FlaskularUser.query.all())
        admin = FlaskularUser.query.filter_by(
            email='admin@example.com'
        ).first()
        assert admin is not None

        # Try to register a duplicate user
        data, status = self.post('/auth/register', email='admin@example.com',
                                 password='bogus')

        assert status == 200
        assert not data['success']
        assert data['code'] == 'register_already_exists'

        # Necessary to prevent an error, but perhaps should be added to
        # model for greater robustness?
        # TODO find a way to remove (exception handoling in model?)
        db.session.rollback()

        # Make sure nothing new was created in the database, and the password
        # remained the same.
        post_count = len(FlaskularUser.query.all())
        assert post_count == count

        user = FlaskularUser.query.filter_by(email='admin@example.com').first()
        assert user is not None

        assert user.email == 'admin@example.com'
        assert verify_password('admin', user.password)

        admin_role = FlaskularRole.query.filter_by(name='admin').first()
        sub_role = FlaskularRole.query.filter_by(name='subscriber').first()

        assert user.has_role(admin_role)
        assert not user.has_role(sub_role)

    def test_token_login(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert data['success']

        # Hopefully, the code executes through here within 1 second
        # (token expiration time in testing environment)
        d2, s2 = self.post('/auth/token-login', token=data['token'])

        assert s2 == 200
        assert d2['success']
        assert d2['code'] == 'token_login_success'
        assert d2['user']['email'] == 'admin@example.com'
        assert d2['token'] == data['token']
        assert len(d2['user']['roles']) == 1
        assert d2['user']['roles'][0] == 'admin'

    def test_token_login_expired(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']

        # Wait for token to expire
        start = time.time()
        time.sleep(2)
        print('elapsed = {}'.format(time.time() - start))
        d2, s2 = self.post('/auth/token-login', token=data['token'])

        assert s2 == 200
        assert not d2['success']
        assert d2['code'] == 'token_login_expired'
        assert d2['user'] == {}
        assert d2['token'] == data['token']

    def test_token_login_invalid(self):
        d2, s2 = self.post('/auth/token-login', token='Bogus')

        assert s2 == 200
        assert not d2['success']
        assert d2['code'] == 'token_login_invalid'
        assert d2['user'] == {}
        assert d2['token'] == 'Bogus'

    def test_token_login_logged_out(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']

        d, s = self.get('/auth/logout')

        d2, s2 = self.post('/auth/token-login', token=data['token'])

        assert s2 == 200
        assert not d2['success']
        assert d2['code'] == 'token_login_logged_out'

    def test_logout(self):
        """
        Make sure the token login fails if the user has already logged out,
        even if the token is valid and hasn't yet expired.
        """
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']
        d, s = self.get('/auth/logout')

        assert s == 200
        assert data['success']

    def test_logout_nobody(self):
        d, s = self.get('/auth/logout')

        assert s == 200
        assert not d['success']
        assert d['code'] == 'logout_nobody'

    def test_logout_nobody_after_login_logout(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert status == 200 and data['success']
        d, s = self.get('/auth/logout')
        assert s == 200 and d['success']

        d2, s2 = self.get('/auth/logout')
        assert s2 == 200
        assert not d2['success']
        assert d2['code'] == 'logout_nobody'

    def test_user_list(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        token = data['token']
        data, status = self.post('/auth/user-list', token=token)
        assert status == 200
        assert data['success']

        userlist = data['userlist']

        # Make sure admin is found
        admin_found = 0
        for user in userlist:
            email = user['email']
            if email == 'admin@example.com':
                roles = user['roles']
                assert len(roles) == 1
                assert roles[0] == 'admin'
                admin_found += 1
        assert admin_found == 1

    def test_user_dict(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        token = data['token']
        data, status = self.post('/auth/user-dict', token=token)

        assert 'userDict' in data
        assert data['userDict']['email'] == 'admin@example.com'
        assert data['userDict']['username'] == 'admin'

    def test_user_change_password(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        token = data['token']
        data, status = self.post('/auth/change-password', token=token,
                                 oldPassword='admin', newPassword='new')
        assert data['success']

        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert not data['success']
        assert data['code'] == 'login_invalid_password'

        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='new')
        assert data['success']

    def test_user_change_password_wrong_old(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        token = data['token']
        data, status = self.post('/auth/change-password', token=token,
                                 oldPassword='bogus', newPassword='new')
        assert not data['success']
        assert data['code'] == 'password_change_incorrect_old'

    def test_update_profile(self):
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        id = data['user']['id']
        token = data['token']
        data, status = self.post('/auth/update-profile', token=token, id=id,
                                 firstName='The', lastName='Admin')

        assert data['success']
        assert data['code'] == 'update_profile_success'

        data, status = self.post('/auth/token-login', token=token)

        assert data['success']
        assert data['user']['id'] == id
        assert data['user']['firstName'] == 'The'
        assert data['user']['lastName'] == 'Admin'

    def test_update_profile_unauthorized(self):

        # Get admin id
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        id = data['user']['id']

        # Create new user
        data, status = self.post('/auth/register', email='test1@example.com',
                                 password='test1')

        # Login as new user
        assert data['success']
        data, status = self.post('/auth/login', email='test1@example.com',
                                 password='test1')
        assert data['success']
        token = data['token']

        # Attempt to change admin's profile from new user
        data, status = self.post('/auth/update-profile', token=token, id=id,
                                 firstName='The', lastName='Admin')
        assert not data['success']
        assert data['code'] == 'update_profile_not_authorized'

    def test_delete_user(self):

        # Create new user
        data, status = self.post('/auth/register', email='test1@example.com',
                                 password='test1')

        # Get id of new user
        assert data['success']
        data, status = self.post('/auth/login', email='test1@example.com',
                                 password='test1')
        assert data['success']
        id = data['user']['id']

        # Login as admin
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert data['success']
        token = data['token']

        # Use admin to delete new user
        data, status = self.post('/auth/delete-user', token=token, id=id)
        assert data['success']
        assert data['code'] == 'delete_success'

        # Make sure new user is still deleted
        data, status = self.post('/auth/login', email='test1@example.com',
                                 password='test1')
        assert not data['success']
        assert data['code'] == 'login_unknown_user'

    def test_promote_demote(self):
        # Create new user
        data, status = self.post('/auth/register', email='test1@example.com',
                                 password='test1')

        # Get id of new user
        assert data['success']
        data, status = self.post('/auth/login', email='test1@example.com',
                                 password='test1')
        assert data['success']
        id = data['user']['id']

        # Login as admin
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert data['success']
        token = data['token']

        # Use admin to promote new user to admin
        data, status = self.post('/auth/promote-user', token=token, id=id)
        assert data['success']
        assert data['code'] == 'promote_success'

        # Check to see if new user has admin role
        data, status = self.post('/auth/user-list', token=token)
        assert data['success']
        found = False
        for user in data['userlist']:
            if user['id'] == id:
                assert 'admin' in user['roles']
                assert 'subscriber' not in user['roles']
                found = True
                break
        assert found

        # Login as admin again to refresh token
        data, status = self.post('/auth/login', email='admin@example.com',
                                 password='admin')
        assert data['success']
        token = data['token']

        # Use admin to demote new user back to subscriber
        data, status = self.post('/auth/demote-user', token=token, id=id)
        print(data)
        assert data['success']
        assert data['code'] == 'demote_success'

        # Check to see if new user no longer has admin role
        data, status = self.post('/auth/user-list', token=token)
        assert data['success']
        found = False
        for user in data['userlist']:
            found = False
            if user['id'] == id:
                assert 'admin' not in user['roles']
                assert 'subscriber' in user['roles']
                found = True
                break
        assert found
