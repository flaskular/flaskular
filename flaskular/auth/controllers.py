from itsdangerous import BadSignature, SignatureExpired

from flask import Blueprint, request

from flaskular.decorators import (json_post, json_response, auth_required,
                                  roles_required)
from flaskular.extensions import RegistrationManager as rm

from .models import flaskular_roles_users  # noqa
from . import exceptions as err
from .codes import AUTH_CODES as c


###############################################################################
#   Configuration
###############################################################################

# Define the blueprint
auth = Blueprint('auth', __name__, url_prefix='/auth')

###############################################################################
#   Routes
###############################################################################


@auth.route('/register', methods=['POST'], overwritable=True)
@json_post('email', 'password')
def register():
    """
    Route to register a new users.

    Post Parameters
    ---------------
    email : str
        The email of the user to register.
    password : str
        The password for the user.

    Returns
    -------
    results : json
        The results of the attempt to register. The keys are as follows:
            * success : bool
                True if the registration was successful, false otherwise.
            * msg : str
                The status message returned by the operation.
    """
    data = request.json
    code = c.unknown()

    try:
        rm.User.create(email=data['email'], password=data['password'])
        code = c.register_success()
    except:
        # TODO better status messages based on db reply
        code = c.register_already_exists(user=data['email'])

    return {
        'code': code,
    }


@auth.route('/login', methods=['POST'], overwritable=True)
@json_post('email', 'password')
def login():
    """
    Route to perform a user login.

    Post Parameters
    ---------------
    email : str
        The email of the user to login.
    password : str
        The password for the user.

    Returns
    -------
    results : json
        The results of the attempt to login. The keys are as follows:
            * success : bool
                True if the login was successful, false otherwise.
            * msg : str
                The status message returned by the operation.
            * id : str
                The user id (email)
            * token : str
                The token which can be used for temporary login without
                requiring a password. See SECURITY_TOKEN_MAX_AGE in the config
                for the lifespan of the token.
    """

    data = request.json
    email = data['email']
    pswd = data['password']

    user_dict = {}
    token = None

    try:
        user = rm.User.login(email, pswd)
        user_dict = user.to_dict()
        token = user.get_auth_token()
        code = c.login_success()
    except err.AuthUserNotFound:
        code = c.login_unknown_user(user=email)
    except err.AuthInvalidPassword:
        code = c.login_invalid_password()
    except Exception:
        code = c.unknown()

    return {
        'code': code,
        'user': user_dict,
        'token': token,
    }


@auth.route('/token-login', methods=['POST'], overwritable=True)
@json_post('token')
def token_login():
    """
    Route to perform a login using a token.

    Post Parameters
    ---------------
    token : str
        The token to use to attempt to login.

    Returns
    -------
    results : json
        The results of the attempt to login. The keys are as follows:
            * success : bool
                Whether or not the token login was successful.
            * message : str
                A message about the success or failure.
            * id : str
                The user id (email).
            * token : str
                The user's temporary login token.
    """
    data = request.json
    token = data['token']

    user_dict = {}

    code = c.unknown()

    try:
        user = rm.User.get_from_token(token)
        user_dict = user.to_dict()
        code = c.token_login_success()
    except SignatureExpired:
        code = c.token_login_expired()
    except BadSignature:
        code = c.token_login_invalid()
    except err.AuthTokenLoggedOut:
        code = c.token_login_logged_out()
    except Exception:
        # Hide token error details from client (such as missing user, bad
        # password, etc).
        code = c.unknown()

    return {
        'code': code,
        'user': user_dict,
        'token': token,
    }


@auth.route('/logout', overwritable=True)
@json_response
def logout():
    """
    Logs out the current user.

    Returns
    -------
    results : json
        The results of the attempt to logout. The keys are as follows:
            * success : bool
                True if the logout attempt was successful
    """
    try:
        email = rm.User.logout()
        code = c.logout_success(user=email)
    except err.AuthLogoutNobody:
        code = c.logout_nobody()
    except Exception:
        code = c.unknown()

    return {
        'code': code,
    }


@auth.route('/user-list', methods=['POST'], overwritable=True)
@json_response
@auth_required()
@roles_required('admin')
def user_list():
    """
    Returns a list of all users
    """
    users = rm.User.query.all()
    user_list = []

    for user in users:
        user_list.append(user.to_dict())

    return {
        'code': c.user_list_success(),
        'userlist': user_list
    }


@auth.route('/user-dict', methods=['POST'], overwritable=True)
@json_response
@auth_required()
def user_dict():
    """
    Gets the current logged in user's dictionary.
    """
    return {
        'code': c.get_user_dict_success(),
        'userDict': request.user.to_dict()
    }


@auth.route('/change-password', methods=['POST'], overwritable=True)
@json_post('oldPassword', 'newPassword')
@auth_required()
def change_password():
    """
    Changes a user's password, making sure the old password is correct first.
    """
    user = request.user

    data = request.json
    old_password = data['oldPassword']
    new_password = data['newPassword']

    code = c.unknown()

    try:
        user.reset_password(old_password, new_password)

        code = c.password_change_success()
    except err.NotAuthorized:
        code = c.password_change_incorrect_old()
    except Exception:
        pass

    return {
        'code': code
    }


@auth.route('/update-profile', methods=['POST'], overwritable=True)
@json_post('id')
@auth_required()
def update_profile():
    """
    Updates a user's profile.
    """
    data = request.json
    code = c.unknown()

    requester = request.user

    user_id = data['id']
    first_name = data.get('firstName', '')
    last_name = data.get('lastName', '')

    try:
        user = rm.User.query.get(user_id)

        if user != requester and not requester.has_role('admin'):
            code = c.update_profile_not_authorized()
        else:
            user.update_profile(first_name=first_name, last_name=last_name)
            code = c.update_profile_success(email=user.email)
    except Exception:
        pass

    return {
        'code': code
    }


@auth.route('/delete-user', methods=['POST'], overwritable=True)
@json_post('id')
@auth_required()
@roles_required('admin')
def delete_user():
    """
    Deletes the user.
    """
    data = request.json
    user_id = data['id']

    code = c.unknown()

    try:
        user = rm.User.query.get(user_id)
        user.delete()

        code = c.delete_success()
    except Exception:
        pass

    return {
        'code': code
    }


@auth.route('/promote-user', methods=['POST'], overwritable=True)
@json_post('id')
@auth_required()
@roles_required('admin')
def promote_user():
    """
    Promotes a user to admin.
    """
    data = request.json
    user_id = data['id']

    code = c.unknown()

    try:
        user = rm.User.query.get(user_id)
        user.promote()
        code = c.promote_success(email=user.email)
    except Exception:
        pass

    return {
        'code': code
    }


@auth.route('/demote-user', methods=['POST'], overwritable=True)
@json_post('id')
@auth_required()
@roles_required('admin')
def demote_user():
    """
    Demotes a user from admin.
    """
    data = request.json
    user_id = data['id']

    code = c.unknown()

    try:
        user = rm.User.query.get(user_id)
        user.demote()
        code = c.demote_success(email=user.email)
    except Exception:
        pass

    return {
        'code': code
    }
