from werkzeug.security import safe_str_cmp

from flask import current_app as app
from flask import session
from flask_security import RoleMixin, UserMixin
from flask_security.utils import (login_user, logout_user,
                                  encrypt_password, md5,
                                  verify_password)

from flaskular.extensions import db, print_warning
from flaskular.extensions import RegistrationManager as rm

from . import exceptions as err


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   Base Model
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

class Base(db.Model):
    """
    Base model common to other models.

    Schema
    ------
    id : int, primary key
    created_at : DateTime
    modified_at : DateTime
    """
    __abstract__ = True

    id = db.Column(db.Integer(), primary_key=True)
    created_at = db.Column(db.DateTime(timezone=True),
                           default=db.func.current_timestamp())
    modified_at = db.Column(db.DateTime(timezone=True),
                            default=db.func.current_timestamp(),
                            onupdate=db.func.current_timestamp())


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   Role Model
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

class FlaskularRoleMixin(Base, RoleMixin):
    """Model defining a user role.

    Schema
    ------
    [inherited from Base]
    name : str, unique, not null
    description : str

    Parameters
    ----------
    name : str
        Sets name in schema.
    """
    # __tablename__ = 'flaskular_role'
    __abstract__ = True

    name = db.Column(db.String(80), nullable=False, unique=True)
    description = db.Column(db.String(255))

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Flaskular Role {}>'.format(self.name)

    ###########################################################################
    #   Static Helpers
    ###########################################################################

    @staticmethod
    def initialize():
        """Initializes the database

        Initializes with the roles `admin` and `subscriber` if they do not
        already exist in the database. Called on application startup.
        """
        roles = {
            'admin': ('Admin users for full privileges.'),
            'subscriber': ('Lowest level privileges, default for' +
                           'first-time registration.')
        }

        for name, description in roles.items():
            if not rm.Role.query.filter_by(name=name).first():
                rm.Role.create(name, description=description)

    @staticmethod
    def create(name, description='', commit=True):
        """Creates and returns a new role.

        Parameters
        ----------
        name : str
            The name of the new role.
        description : str, default=''
            The (optional) description of the new role.
        commit : bool, default=True
            Whether the new role should be committed to the database upon
            creation.

        Returns
        -------
        role : Role
            The new role.
        """
        security = app.extensions['security']
        role = security.datastore.create_role(name=name)
        role.description = description

        if commit:
            db.session.commit()
        else:
            db.session.close()

        return role


# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   User Model
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

class FlaskularUserMixin(Base, UserMixin):
    """Model defining a user.

    Schema
    ------
    [inherited from Base]
    email : str, unique, not null
    password : str, not null
    first_name : str
    last_name : str
    active : bool
    confirmed_at : DateTime
    last_login_at : DateTime
    current_login_at : DateTime
    last_login_ip : str
    login_count : int
    logged_out : boolean
        Tracks whether a logout has recently been executed. Used to prevent
        token logins when the user has manually logged out.
    roles : relationship (many-to-many with role)
    """
    # __tablename__ = 'flaskular_user'
    __abstract__ = True

    email = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)

    first_name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))

    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime(timezone=False))
    last_login_at = db.Column(db.DateTime(timezone=False))
    current_login_at = db.Column(db.DateTime(timezone=False))
    last_login_ip = db.Column(db.String(45))
    current_login_ip = db.Column(db.String(45))
    login_count = db.Column(db.Integer())
    logged_out = db.Column(db.Boolean(), default=True)

    # @declared_attr
    # def roles(cls):
    #     return db.relationship(rm.Role.__name__,
    #                            secondary=roles_users,
    #                            backref=db.backref('users', lazy='dynamic'))

    def __repr__(self):
        return '<Flaskular User {}>'.format(self.email)

    ###########################################################################
    #   Methods
    ###########################################################################

    def to_dict(self):
        """Returns a dictionary representation of this user.

        Returns
        -------
        user_dict : dict
        """
        return {
            'id': self.id,
            'email': self.email,
            'username': self.email.partition('@')[0],
            'firstName': self.first_name,
            'lastName': self.last_name,
            'createdAt': self.created_at,
            'modifiedAt': self.modified_at,
            'lastLoginAt': self.last_login_at,
            'currentLoginAt': self.current_login_at,
            'roles': [role.name for role in self.roles]
        }

    def set_password(self, password):
        """Encrypts and sets the user's password.

        Parameters
        ----------
        password : str
            The (unencrypted) password to set.
        """
        self.password = encrypt_password(password)

        db.session.commit()

    def reset_password(self, old_pswd, new_pswd):
        """Resets the user's password.

        Must use the old password to verify for added security.

        Raises
        ------
        NotAuthorized
            If old_pswd does not match the user's current password.

        Parameters
        ----------
        old_pswd : str
            The current password.
        new_pswd : str
            The new password.
        """
        if not verify_password(old_pswd, self.password):
            raise err.NotAuthorized()

        self.set_password(new_pswd)

    def update_profile(self, **kwargs):
        """Updates this user's profile.

        Possible kwargs
        ---------------
        first_name : str
        last_name : str
        """
        self.first_name = kwargs.get('first_name', '')
        self.last_name = kwargs.get('last_name', '')

        db.session.commit()

    def promote(self):
        """Promotes the user.

        Adds the admin role if the user doesn't already have it. Removes the
        subscriber role if the user has it.
        """
        security = app.extensions['security']
        admin_role = security.datastore.find_role('admin')
        subscriber_role = security.datastore.find_role('subscriber')

        if not self.has_role(admin_role):
            security.datastore.add_role_to_user(self, admin_role)

        if self.has_role(subscriber_role):
            security.datastore.remove_role_from_user(self, subscriber_role)

        db.session.commit()

    def demote(self):
        """Demotes the user.

        Removes the admin role if the already has it. Adds the subscriber role
        if the user does not have it.
        """
        security = app.extensions['security']
        admin_role = security.datastore.find_role('admin')
        subscriber_role = security.datastore.find_role('subscriber')

        if self.has_role(admin_role):
            security.datastore.remove_role_from_user(self, admin_role)

        if not self.has_role(subscriber_role):
            security.datastore.add_role_to_user(self, subscriber_role)

        db.session.commit()

    def delete(self):
        """Deletes this user.
        """
        db.session.delete(self)
        db.session.commit()

    ###########################################################################
    #   Static Helpers
    ###########################################################################

    @staticmethod
    def login(user_id, password):
        """Logs the user in with the given password.

        Parameters
        ----------
        user_id : str
            The user's id (email address)
        password : str
            The user's password. To be checked against the database-stored
            and hashed password.
        Raises
        ------
        AuthDualLogin
            When the session has stored another login
        AuthUserNotFound
            When no user with email `user_id` was found
        AuthInvalidPassword
            When the password `password` is incorrect for the found user.

        Returns
        -------
        token : str
            The user's login token.
        """
        user = rm.User.query.filter_by(email=user_id).first()

        if user is None:
            raise err.AuthUserNotFound(user_id)

        if 'user_id' in session and int(session['user_id']) != int(user.id):
            other = rm.User.query.get(session['user_id'])
            if other is not None:
                other.logout()
            # raise err.AuthDualLogin(other.email)

        if not verify_password(password, user.password):
            raise err.AuthInvalidPassword()

        login_user(user)
        user.logged_out = False
        db.session.commit()

        return user

    @staticmethod
    def logout():
        """Logs the current user out.

        Raises
        ------
        AuthLogoutNobody
            When a logout attempt is made with nobody logged in.

        Returns
        -------
        email : str
            The email of the user.
        """
        if 'user_id' not in session:
            raise err.AuthLogoutNobody()

        user = rm.User.query.get(session['user_id'])
        user.logged_out = True

        logout_user()
        db.session.commit()

        return user.email

    @staticmethod
    def get_current_user():
        """Returns the current logged in user.

        Returns
        -------
        user : User
            The current logged in user.
        """
        if 'user_id' not in session:
            return None

        id = session['user_id']
        return rm.User.query.get(id)

    @staticmethod
    def initialize():
        """Initializes by creating the default users.

        Creates a user with id=admin@example.com, password=admin if no user
        already exists. The admin user is not required, so that it will not
        be recreated if deleted, so long as other users exist.

        Called on application startup.

        Prints a warning to the server log if the admin user is using the
        default password.
        """

        # TODO filter by admins so that it is guaranteed that an admin user
        # exists
        if not rm.User.query.first():
            rm.User.create('admin@example.com', 'admin', roles='admin')

        rm.User._warning_default_password('admin@example.com', 'admin')

    @staticmethod
    def _warning_default_password(uname, default_password, required=False):
        """Displays a warning to the server log if default users still use
        default passwords.

        Parameters
        ----------
        uname : str
            The user name
        default_password : str
            The password to check. A warning is shown if the user can be
            authenticated with this password.
        required : bool
            True if the user is required to exist. If the user is required
            but does not exist, the server will crash. As such, the initialize
            method should ensure that the user exists before calling this
            method.
        """
        user = rm.User.query.filter_by(email=uname).first()

        if not required and not user:
            return  # pragma: no cover

        if required and not user:                            # pragma: no cover
            msg = 'Critical error, user {} does not exist'
            msg = msg.format(uname)
            raise Exception(msg)

        if verify_password(default_password, user.password):
            msg = (('Warning! Default administrative user "{}" is still '
                    'using the default password "{}". For security '
                    'reasons, please log in to the client and '
                    'change this password.').format(uname,
                                                    default_password))
            print_warning(msg)

    @staticmethod
    def create(email, password, roles='subscriber', commit=True):
        """Static helper that creates and returns a new user of the given role.

        Parameters
        ----------
        email : str
            The email identifying the new user
        password : str
            The (unencrypted) password for the new user
        roles : str or list, def='subscriber'
            The name of the role to assign to the user. Must be an entry in the
            roles database.

            If a list is given, all roles in the list are added to the user.
        commit : bool, def=True
            True if the new user should be committed to the database before
            being returned.

        Returns
        -------
        user : User
            The new user object.
        """
        security = app.extensions['security']

        if isinstance(roles, str):
            roles = [roles]

        user = security.datastore.create_user(
            email=email,
            password=encrypt_password(password),
            roles=roles
        )
        if commit:
            db.session.commit()
        else:
            db.session.close()

        """
        role = Role.query.filter_by(name=role_name).first()
        security.datastore.add_role_to_user(user, role)

        if commit:
            db.session.commit()
        """
        return user

    @staticmethod
    def get_from_token(token):
        """
        Attempts to load a user based on a token.

        Parameters
        ----------
        token : str
            The token to verify.

        Raises
        ------
        itsdangerous.BadSignature
            If the token is not valid.
        itsdangerous.SignatureExpired
            If the token has expired.
        AuthTokenNoUser
            If the user on the token does not exist.
        AuthTokenLoggedOut
            If the user has manually logged out and attempts to use the token.
        Exception
            If an unknown error occurred.

        Returns
        -------
        user : User
            The user corresponding to the token.
        """
        security = app.extensions['security']
        serializer = security.remember_token_serializer
        data = serializer.loads(token, max_age=security.token_max_age)

        if data[0] is None or data[0] == 'None':
            raise err.AuthTokenNoUser()

        try:
            id = int(data[0])
        except:  # pragma: no cover
            raise err.AuthTokenNoUser()

        user = security.datastore.find_user(id=id)
        if not user:  # pragma: no cover
            raise err.AuthTokenNoUser()
        if not safe_str_cmp(md5(user.password), data[1]):
            raise err.AuthTokenInvalid()
        if user.logged_out:
            raise err.AuthTokenLoggedOut()

        login_user(user)  # Make sure user is logged in to this session
        return user

# 00000000000000000000000000000000000000000000000000000000000000000000000000000
#   Flaskular test inherited
# 00000000000000000000000000000000000000000000000000000000000000000000000000000

"""
Helper table creating many-to-many relationships between users and roles.

Schema
------
user_id : int, foreign key (auth_user.id)
role_id : int, foreign key (auth_role.id)
"""
flaskular_roles_users = db.Table(
    'flaskular_roles_users',
    db.Column('user_id', db.Integer(), db.ForeignKey('flaskular_user.id')),
    db.Column('role_id', db.Integer(), db.ForeignKey('flaskular_role.id'))
)


class FlaskularRole(FlaskularRoleMixin):
    """A default Role.

    Should only be used in Flaskular tests.
    """
    __tablename__ = 'flaskular_role'


class FlaskularUser(FlaskularUserMixin):
    """A default, non-custom User.

    Should only be used in Flaskular test cases.
    """
    __tablename__ = 'flaskular_user'

    # Must link Users <-> Roles in inherited classes
    roles = db.relationship('FlaskularRole',
                            secondary=flaskular_roles_users,
                            backref=db.backref('users', lazy='dynamic'))
