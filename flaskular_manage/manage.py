"""
Manages the database migrations using Alembic and Flask-Migrate. See README.md
for usage instructions.
"""

from flask.ext.script import Manager
from flask.ext.migrate import Migrate, MigrateCommand


def build_manager(app, db):
    Migrate(app, db)
    manager = Manager(app)

    manager.add_command('db', MigrateCommand)

    return manager
