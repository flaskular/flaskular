from .setup import (Install as FlaskularInstall,    # noqa
                    Develop as FlaskularDevelop,    # noqa
                    PyTest as FlaskularPyTest,      # noqa
                    PyDoc as FlaskularPyDoc)        # noqa
