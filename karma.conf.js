// Karma configuration
// Generated on Wed Jan 06 2016 11:03:50 GMT-0700 (MST)

module.exports = function(config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],


    // list of files / patterns to load in the browser
    files: [

      // Flaskular library js
      'flaskular/jslib/angular/angular.js',
      'flaskular/jslib/angular-mocks/angular-mocks.js',
      'flaskular/jslib/angular-bootstrap/ui-bootstrap.js',
      'flaskular/jslib/angular-bootstrap/ui-bootstrap-tpls.js',
      'flaskular/jslib/angular-ui-router/release/angular-ui-router.js',
      'flaskular/jslib/ngstorage/ngStorage.js',
      'flaskular/jslib/sinonjs/sinon.js',
      'flaskular/jslib/jasmine-sinon/lib/jasmine-sinon.js',
      'flaskular/jslib/angular-cookies/angular-cookies.js',
      'flaskular/jslib/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',

      // Flaskular core js
      'flaskular/static/flaskular.module.js',
      'flaskular/static/**/*.module.js',
      'flaskular/static/**/!(*.spec).js',
      'flaskular/static/**/*.spec.js'
    ],


    // list of files to exclude
    exclude: [
    ],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'flaskular/static/**/!(*.spec|router.*|*.routes|*.module).js': ['coverage']
    },


    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],

    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity,

    coverageReporter: {
        type: 'text'
    }
  });
};
